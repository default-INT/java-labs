package by.gstu.main;

import java.util.List;
import java.util.Scanner;

import by.gstu.entity.ElectricalAppliance;

public class Program {

	private static Scanner in;

	public static void main(String[] args) throws Throwable {
		// TODO Auto-generated method stub
		int n;
		HomeAppliances homeAppliances = new HomeAppliances();
		in = new Scanner(System.in);
		int k = 0;
		while (k != 9) {
			menu();
			k = in.nextInt();
			switch (k) {
			case 1:
				System.out.println(homeAppliances.toString());
				break;
			case 2:
				System.out.print("������� ���: ");
				String type = in.next();
				System.out.print("������� ������: ");
				String model = in.next();
				System.out.print("������� ��������: ");
				double p = in.nextDouble();
				ElectricalAppliance electricalAppliance = 
						HomeAppliances.getAppliance(type, model, p);
				if (electricalAppliance != null)
					homeAppliances.add(electricalAppliance);
				else System.out.println("��� ������ ���� ��������������.");
				break;
			case 3:
				System.out.print("������� ����� ��������������: ");
				int index = in.nextInt();
				if (homeAppliances.delete(index)) 
					System.out.println("������������� ������� ��������.");
				else
					System.out.println("�� ������� �������� �������������.");
				break;
			case 4:
				System.out.print("������� ����� ��������������: ");
				n = in.nextInt();
				if (homeAppliances.onAppliances(n)) 
					System.out.println("������������� ��������� � �������.");
				else 
					System.out.println("�� ������� ���������� ������������� � �������.");
				break;
			case 5:
				System.out.print("������� ����� ��������������: ");
				n = in.nextInt();
				if (homeAppliances.offAppliances(n)) 
					System.out.println("������������� �������� �� �������.");
				else
					System.out.println("�� ������� ��������� �������������.");
				break;
			case 6:
				homeAppliances.sortAppliance();
				System.out.println("�������������� ������� �������������.");
				break;
			case 7:
				System.out.print("������� p1: ");
				double p1 = in.nextDouble();
				System.out.print("������� p2: ");
				double p2 = in.nextDouble();
				List<ElectricalAppliance> eAppliances = homeAppliances.findToP(p1, p2);
				for (ElectricalAppliance appliance : eAppliances) {
					System.out.println(appliance.getType() + " | " + appliance.getP() + " | "
							+ appliance.status());
				}
				break;
			case 8:
				System.out.println("������������ �������� ����� ���������������� �����: "
						+ homeAppliances.getSumP());
				break;
			case 9:
				homeAppliances.finalize();
				System.out.println("��� �������������� ������� ��������� � �������� � ����.");
				break;
			default:
				System.out.println("��� ������ ������.");
				break;
			}
		}
	}
	
	public static void menu() {
		System.out.println("\n--------------------------------\n");
		System.out.println("1) ������� ��� ��������������.");
		System.out.println("2) �������� �������������.");
		System.out.println("3) ������� �������������.");
		System.out.println("4) �������� ������������� N � �������.");
		System.out.println("5) ��������� ������������� N c �������.");
		System.out.println("6) ������������� ��������������.");
		System.out.println("7) ����� ������������� � ��������� ��������.");
		System.out.println("8) ������������ ��������.");
		System.out.println("9) ��������� � �����.");
		System.out.print("-> ");
	}

}
