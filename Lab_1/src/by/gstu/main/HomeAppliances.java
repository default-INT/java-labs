package by.gstu.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import by.gstu.entity.ElectricalAppliance;
import by.gstu.entity.HairDryer;
import by.gstu.entity.Microwave;

public class HomeAppliances {
	
	private final String saveFile = "src/by/gstu/resources/data.txt";
	
	@Override
	public String toString() {
		String out = "";
		int i = 1;
		for (ElectricalAppliance electricalAppliance : electricalAppliances) {
			out += i + ") " + electricalAppliance.getType() + 
					" | P = " + electricalAppliance.getP() + " �� | "
							+ electricalAppliance.status() +"\n";
			i++;
		}
		return out;
	}

	List<ElectricalAppliance> electricalAppliances;
	
	public List<ElectricalAppliance> getApp(){
		return electricalAppliances;
	}
	
	public HomeAppliances() throws IOException {
		electricalAppliances = new ArrayList<ElectricalAppliance>();
		readToFile(saveFile);
	}
	
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		writeToFile(saveFile);
		super.finalize();
	}
	
	public boolean add(ElectricalAppliance electricalAppliance) {
		try {
			electricalAppliances.add(electricalAppliance);
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public boolean delete(int index) {
		try {
			electricalAppliances.remove(index-1);
			return true;
		}catch (Exception e) {
			return false;
		}
		
	}
	
	public void sortAppliance() {
		Collections.sort(electricalAppliances);

	}
	
	
	
	public boolean onAppliances(int n) {
		try {
			electricalAppliances.get(n-1).onPower();
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean offAppliances(int n) {
		try {
			electricalAppliances.get(n-1).offPower();
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public List<ElectricalAppliance> findToP(double np, double kp) {
		List<ElectricalAppliance> eAppliances = new ArrayList<ElectricalAppliance>();
		for (ElectricalAppliance electricalAppliance : electricalAppliances) {
			if (electricalAppliance.getP() > np && electricalAppliance.getP() < kp)
				eAppliances.add(electricalAppliance);
		}
		return eAppliances;
	}
	
	public double getSumP() {
		double sumP = 0;
		for (ElectricalAppliance eAppliance : electricalAppliances) {
			if (eAppliance.isPower()) sumP += eAppliance.getP();
		}
		return sumP;
	}
	
	public static ElectricalAppliance getAppliance(String type, String model, double p) {
		type = type.toLowerCase();
		switch(type) {
		case "�������������":
			return new Microwave(model, p);
		case "���":
			return new HairDryer(model, p);
		default:
			break;
		}
		return null;
	}
	
	private boolean writeToFile(String way) throws IOException {
		try(FileWriter fileWriter = new FileWriter(way, false)){
			for (ElectricalAppliance eAppliance : electricalAppliances) {
				fileWriter.write(eAppliance.toString() + "\n");
			}
			return true;
		}catch (Exception e) {
			return false;
		}
		
	}
	
	private boolean readToFile(String way)
			throws IOException {
		try (FileReader fileReader = new FileReader(way)) {
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			while (line != null) {
				String[] lines = line.split(" ");
				ElectricalAppliance eAppliance = getAppliance(lines[0], lines[1], 
															  Double.parseDouble(lines[2]));
				add(eAppliance);
				line = reader.readLine();
			}
			return true;
		}catch (Exception e) {
			return false;
		}
		
	}
}
