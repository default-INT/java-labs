package by.gstu.entity;

public abstract class ElectricalAppliance implements Comparable<ElectricalAppliance> {

	/**
	 * @param model - ������ ��������������
	 * @param p - ��������
	 */
	public ElectricalAppliance(String model, double p) {
		super();
		this.model = model;
		this.p = p;
	}

	protected String model;
	protected boolean power;
	protected double p;
	
 	public abstract String status();
 	public abstract String getType();
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public boolean isPower() {
		return power;
	}
	
	public void onPower() {
		power = true;
	}
	
	public void offPower() {
		power = false;
	}
	
	public double getP() {
		return p;
	}
	
	public void setP(double p) {
		this.p = p;
	}
	
	@Override
	public int compareTo(ElectricalAppliance elApp) {
		// TODO Auto-generated method stub
		return (int) (this.getP() - elApp.getP());
	}
	
	
}
