package by.gstu.entity;

public class Microwave extends ElectricalAppliance {

	public Microwave(String model, double p) {
		super(model, p);
	}

	@Override
	public String toString() {
		return "������������� " + getModel() + " " + getP();
	}

	@Override
	public String status() {
		// TODO Auto-generated method stub
		if (isPower()) return "���������� � �������";
		else return "��� ����������� � ����";
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "������������� " + getModel();
	}
}
