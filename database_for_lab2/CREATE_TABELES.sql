USE cars_rental;

CREATE TABLE cars
(
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    model varchar(30) NOT NULL,
    mileage INT NOT NULL,
    year_of_issue INT NOT NULL,
    price_hour double NOT NULL
);

CREATE TABLE clients
(
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    fullname varchar(50) NOT NULL,
    bithday_year INT NOT NULL,
    rating INT NOT NULL
);

CREATE TABLE orders
(
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    client_id INT NOT NULL,
    car_id INT NOT NULL,
    passport_data varchar(9) NOT NULL,
    date_order date NOT NULL,
    rental_period INT NOT NULL,
    price double NOT NULL,
    FOREIGN KEY (client_id)  REFERENCES clients (id),
    FOREIGN KEY (car_id)  REFERENCES cars (id)
);