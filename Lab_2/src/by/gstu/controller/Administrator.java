package by.gstu.controller;

import by.gstu.model.*;
import by.gstu.model.entity.Car;
import by.gstu.model.entity.Client;
import by.gstu.model.entity.Order;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Administrator {

    private DAOFactory daoFactory;

    public Administrator(String db){
        daoFactory = DAOFactory.getDAOFactory(db.toUpperCase());
    }

    public String addOrder(int carId, int clientId, String passportData, Date dateOrder,
                            int rentalPeriod) {
        ClientDAO clientDAO = daoFactory.getClientDAO();
        Client client = clientDAO.read(clientId);

        CarDAO carDAO = daoFactory.getCarDAO();
        Car car = carDAO.read(carId);

        String msg = "Неудалось оформить аренду.";
        double price = 0;
        Order order = null;
        if (client != null && car != null) {
            price = car.getPriceHour() * rentalPeriod;
            order = client.getRating() > 0 ?
                    new Order(0, carId, clientId, passportData, dateOrder, rentalPeriod, price) : null;
        }
        if (client.getRating() > 0 && daoFactory.getOrderDAO().create(order)){
            msg = "Аренда автомобиля \"" + carDAO.read(carId).getModel() + "\" успешно оформлена, стоимость аренды " + price;
        }
        return msg;
    }

    public List<Car> getCars() {
        CarDAO carDAO = daoFactory.getCarDAO();
        return carDAO.readAll();
    }

    private List<Client> getClients() {
        return daoFactory.getClientDAO().readAll();
    }

    public String getClientsList() {
        List<Client> clients = getClients();
        String out = "";
        for (Client client : clients) {
            out += client.getId() + ") " + client.getFullName() + " " + client.getBithdayYear()
                    + " rate: " + client.getRating() + "\n";
        }
        return out;
    }

    public String getCarsList() {
        List<Car> cars = daoFactory.getCarDAO().readAll();
        String out = "";
        for (Car car : cars) {
            out += car.getId() + ") " + car.getModel() + " " + car.getYearOfIssue() + "г. " +
                    car.getMileage() + "км. " + car.getPriceHour() + " $\n";
        }
        return out;
    }

    public String getOrdersList() {
        List<Order> orders = daoFactory.getOrderDAO().readAll();
        String out = "";
        SimpleDateFormat formatForDate = new SimpleDateFormat("dd.mm.yyyy");
        for (Order order : orders) {
            out += order.getId() + ") " + formatForDate.format(order.getDateOrder()) +
                    " " + order.getPassportData() + " " + order.getCar(daoFactory).getModel() +
                    " " + order.getClient(daoFactory).getFullName() + " " + order.getPrice() + " $\n";
        }
        return out;
    }

    public String returnCar(int orderId, Date returnDate, double damage) {
        Order order = daoFactory.getOrderDAO().read(orderId);
        String msg = "";
        if (order != null) {
            int days = order.getRentalPeriod() / 24;
            Date date = order.getDateOrder();
            Calendar instance = Calendar.getInstance();
            instance.setTime(date);
            instance.add(Calendar.DAY_OF_MONTH, days);
            date = instance.getTime();
            int compareResult = date.compareTo(order.getDateOrder());
            if (compareResult > 0) {
                msg += "Автомобиль возвращён раньше срока.";
            }else if (compareResult < 0) {
                msg += "Автомобиль возвращён позже срока.";
            }else {
                msg += "Автомобиль возвращён вовремя.";
            }
            if (damage > 0) {
                msg += " Автомобиль повреждён, штраф: " + (damage * 100 / order.getCar(daoFactory).getPriceHour()) + " $.";
            }
        }
        if (daoFactory.getOrderDAO().delete(orderId))
            msg += " Заказ успешно закрыт.";
        else
            msg += " При закрытии заказа произошла ошибка.";
        return msg;
    }
}
