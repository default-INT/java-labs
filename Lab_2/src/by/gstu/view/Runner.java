package by.gstu.view;

import by.gstu.controller.Administrator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Runner {

    private static Scanner in;

    public static void main(String[] args) throws ParseException {
        Administrator administrator;

        in = new Scanner(System.in);
        System.out.print("Введите БД: ");
        String db = in.next();
        administrator = new Administrator(db.toUpperCase());
        int k = 0;
        while (k != 5) {
            menu();
            k = in.nextInt();
            switch (k){
                case 1:
                    System.out.println("Выберите клиента из списка: ");
                    System.out.println(administrator.getClientsList());
                    int clientId = in.nextInt();

                    System.out.println("Выберите автомобиль: ");
                    System.out.println(administrator.getCarsList());
                    int carId = in.nextInt();

                    System.out.print("Введите паспортные данные: ");
                    String passortData = in.next();

                    System.out.print("Введите дату: ");
                    String dateStr = in.next();

                    System.out.print("Введите период аренды, в часах: ");
                    int rentalPeriod = in.nextInt();

                    Date dateOrder = new SimpleDateFormat("dd.mm.yyyy").parse(dateStr);
                    System.out.println(administrator.addOrder(carId, clientId, passortData, dateOrder, rentalPeriod));
                    break;
                case 2:
                    System.out.println("Выберите заказ: ");
                    String msg = administrator.getOrdersList();
                    if (msg == null)
                        break;
                    System.out.println(msg);
                    int id = in.nextInt();

                    System.out.print("Дата возврата: ");
                    String returnDateStr = in.next();
                    Date returnDate = new SimpleDateFormat("dd.mm.yyyy").parse(returnDateStr);

                    System.out.print("Степень повреждения (от 0% до 100%): ");
                    double damage = in.nextDouble();
                    System.out.println(administrator.returnCar(id, returnDate, damage));
                    break;
                case 3:
                    System.out.print(administrator.getClientsList());
                    break;
                case 4:
                    System.out.print(administrator.getOrdersList());
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Нет такого пункта.");
            }
        }
    }

    private static void menu() {
        System.out.println("---------------------------------------");
        System.out.println("|               МЕНЮ                  |");
        System.out.println("---------------------------------------");
        System.out.println("| 1) Взять автомобиль в аренду.       |");
        System.out.println("| 2) Возврат автомобиля.              |");
        System.out.println("| 3) Рейтинг клиентов.                |");
        System.out.println("| 4) Список заказов.                  |");
        System.out.println("---------------------------------------");
        System.out.println("|               ВЫХОД                 |");
        System.out.println("---------------------------------------");
        System.out.print("-> ");
    }
}
