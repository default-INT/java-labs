package by.gstu.model.entity;

public class Car extends Entity {
	
	/**
	 * @param id
	 * @param model
	 * @param mileage
	 * @param yearOfIssue
	 * @param priceHour
	 */
	public Car(int id, String model, int mileage, int yearOfIssue, double priceHour) {
		super(id);
		this.model = model;
		this.mileage = mileage;
		this.yearOfIssue = yearOfIssue;
		this.priceHour = priceHour;
	}
	
	public Car(int id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	private String model;
	private int mileage;
	private int yearOfIssue;
	private double priceHour;
	
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getMileage() {
		return mileage;
	}
	public void setMileage(int mileage) {
		this.mileage = mileage;
	}
	public int getYearOfIssue() {
		return yearOfIssue;
	}
	public void setYearOfIssue(int yearOfIssue) {
		this.yearOfIssue = yearOfIssue;
	}
	public double getPriceHour() {
		return priceHour;
	}
	public void setPriceHour(double priceHour) {
		this.priceHour = priceHour;
	}

	@Override
	public String toString() {
		return getId() + "_" + getModel() + "_" + getMileage() + "_" + getYearOfIssue() + "_" + getPriceHour();
	}
}
