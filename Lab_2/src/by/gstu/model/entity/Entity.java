/**
 * 
 */
package by.gstu.model.entity;

/**
 * @author default-INT
 *
 */
public abstract class Entity {
	
	/**
	 * @param id entity
	 */
	public Entity(int id) {
		super();
		Id = id;
	}
	
	public Entity() {
		
	}

	private int Id;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	} 
}
