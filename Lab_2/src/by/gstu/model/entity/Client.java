package by.gstu.model.entity;

public class Client extends Entity {
	/**
	 * @param id
	 * @param fullName
	 * @param bithdayYear
	 * @param rating
	 */
	public Client(int id, String fullName, int bithdayYear, int rating) {
		super(id);
		this.fullName = fullName;
		this.bithdayYear = bithdayYear;
		this.rating = rating;
	}
	/**
	 * @param id
	 */
	public Client(int id) {
		super(id);
		// TODO Auto-generated constructor stub
	}
	private String fullName;
	private int bithdayYear;
	private int rating;
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public int getBithdayYear() {
		return bithdayYear;
	}
	public void setBithdayYear(int bithdayYear) {
		this.bithdayYear = bithdayYear;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return getId() + "_" + getFullName() + "_" + getBithdayYear() + "_" + getRating();
	}
}
