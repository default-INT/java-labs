/**
 * 
 */
package by.gstu.model.entity;

import by.gstu.model.DAOFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author default-INT
 *
 */
public class Order extends Entity {

	/**
	 * @param id
	 * @param clientId
	 * @param carId
	 * @param passportData
	 * @param dateOrder
	 * @param rentalPeriod
	 * @param price
	 */
	public Order(int id, int clientId, int carId, String passportData, Date dateOrder, int rentalPeriod, double price) {
		super(id);
		this.clientId = clientId;
		this.carId = carId;
		this.passportData = passportData;
		this.dateOrder = dateOrder;
		this.rentalPeriod = rentalPeriod;
		this.price = price;
	}
	
	/**
	 * @param id
	 */
	public Order(int id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	private int clientId;
	private Client client;
	
	private int carId;
	private Car car;
	
	private String passportData;
	private Date dateOrder;
	private int rentalPeriod;
	private double price;
	
	
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getCarId() {
		return carId;
	}
	public void setCarId(int carId) {
		this.carId = carId;
	}
	public String getPassportData() {
		return passportData;
	}
	public void setPassportData(String passportData) {
		this.passportData = passportData;
	}
	public Date getDateOrder() {
		return dateOrder;
	}
	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}
	public int getRentalPeriod() {
		return rentalPeriod;
	}
	public void setRentalPeriod(int rentalPeriod) {
		this.rentalPeriod = rentalPeriod;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the car
	 */
	public Car getCar(DAOFactory daoFactory) {
		return daoFactory.getCarDAO().read(carId);
	}

	/**
	 * @param car the car to set
	 */
	public void setCar(Car car) {
		this.car = car;
	}

	/**
	 * @return the client
	 */
	public Client getClient(DAOFactory daoFactory) {
		//DAOFactory daoFactory = DAOFactory.getDAOFactory();
		return daoFactory.getClientDAO().read(clientId);
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public String toString() {
        SimpleDateFormat formatForDate = new SimpleDateFormat("dd.mm.yyyy");
		return getId() + "_" + getClientId() + "_" + getCarId() + "_" + getPassportData() + "_" +
				formatForDate.format(getDateOrder()) + "_" + getRentalPeriod() + "_" + getPrice();
	}
	/**
	 * @param id
	 * @param clientId
	 * @param carId
	 * @param passportData
	 * @param dateOrder
	 * @param rentalPeriod
	 * @param price
	 */
}
