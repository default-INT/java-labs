package by.gstu.model.filesystem;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import by.gstu.model.CarDAO;
import by.gstu.model.entity.Car;
import by.gstu.model.mysql.ConstConfig;

public class FSCarDAO implements CarDAO {

	private static final String WAY = ConstConfig.FS_CARS;

	public FSCarDAO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Car car) {
		List<Car> cars = readAll();
		int maxId = 1;
		if (cars != null){
			for (Car iCar : cars){
				maxId = iCar.getId() > maxId ? iCar.getId() : maxId;
			}
		}else {
			cars = new ArrayList<Car>();
		}
		car.setId(++maxId);
		cars.add(car);
		return writeToFile(cars);
	}

	@Override
	public boolean update(Car car) {
		List<Car> cars = readAll();
		for (int i = 0; i < cars.size(); i++){
			if (cars.get(i).getId() == car.getId() &&
			    cars.get(i).getModel().equals(car.getModel())){
				cars.set(i, car);
				return writeToFile(cars);
			}
		}
		return false;
	}

	@Override
	public Car read(int id) {
		try (FileReader fileReader = new FileReader(WAY)){
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			while (line != null) {
				String[] lines = line.split("_");
				int idLines = Integer.parseInt(lines[0]);
				if (id == idLines){
					String model = lines[1];
					int mileage = Integer.parseInt(lines[2]);
					int yearOfIssue = Integer.parseInt(lines[3]);
					double priceHour = Double.parseDouble(lines[4]);
					return new Car(id, model, mileage, yearOfIssue,priceHour);
				}
				line = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		List<Car> cars = readAll();
		if (cars != null){
			for (int i = 0; i < cars.size(); i++){
				if (cars.get(i).getId() == id){
					cars.remove(i);
					return writeToFile(cars);
				}
			}
		}
		return false;
	}

	@Override
	public boolean delete(Car car) {
		return delete(car.getId());
	}

	@Override
	public List<Car> readAll() {
		try (FileReader fileReader = new FileReader(WAY)){
			List<Car> cars = new ArrayList<Car>();
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			while (line != null) {
				String[] lines = line.split("_");
				int id = Integer.parseInt(lines[0]);
				String model = lines[1];
				int mileage = Integer.parseInt(lines[2]);
				int yearOfIssue = Integer.parseInt(lines[3]);
				double priceHour = Double.parseDouble(lines[4]);
				cars.add(new Car(id, model, mileage, yearOfIssue,priceHour));
				line = reader.readLine();
			}
			return cars;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean writeToFile(List<Car> cars){
		try(FileWriter fileWriter = new FileWriter(WAY, false)){
			for (Car car : cars) {
				fileWriter.write(car.toString() + "\n");
			}
			return true;
		}catch (Exception e) {
			return false;
		}
	}
}
