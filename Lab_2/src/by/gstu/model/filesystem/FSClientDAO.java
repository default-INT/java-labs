package by.gstu.model.filesystem;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import by.gstu.model.ClientDAO;
import by.gstu.model.entity.Client;
import by.gstu.model.mysql.ConstConfig;

public class FSClientDAO implements ClientDAO {

	private static final String WAY = ConstConfig.FS_CLIENTS;

	public FSClientDAO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Client client) {
		List<Client> clients = readAll();
		int maxId = 1;
		if (clients != null){
			for (Client iClients : clients){
				maxId = iClients.getId() > maxId ? iClients.getId() : maxId;
			}
		}else {
			clients = new ArrayList<Client>();
		}
		client.setId(++maxId);
		clients.add(client);
		return writeToFile(clients);
	}

	@Override
	public boolean update(Client client) {
		List<Client> clients = readAll();
		for (int i = 0; i < clients.size(); i++){
			if (clients.get(i).getId() == client.getId() &&
					clients.get(i).getFullName().equals(client.getFullName())){
				clients.set(i, client);
				return writeToFile(clients);
			}
		}
		return false;
	}

	@Override
	public Client read(int id) {
		try (FileReader fileReader = new FileReader(WAY)){
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			while (line != null) {
				String[] lines = line.split("_");
				int idLines = Integer.parseInt(lines[0]);
				if (idLines == id) {
					String fullName = lines[1];
					int bithdayYear = Integer.parseInt(lines[2]);
					int rating = Integer.parseInt(lines[3]);
					return new Client(id, fullName, bithdayYear, rating);
				}
				line = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		List<Client> clients = readAll();
		if (clients != null){
			for (int i = 0; i < clients.size(); i++){
				if (clients.get(i).getId() == id){
					clients.remove(i);
					return writeToFile(clients);
				}
			}
		}
		return false;
	}

	@Override
	public boolean delete(Client client) {
		return delete(client.getId());
	}

	@Override
	public List<Client> readAll() {
		try (FileReader fileReader = new FileReader(WAY)){
			List<Client> clients = new ArrayList<Client>();
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			while (line != null) {
				String[] lines = line.split("_");
				int id = Integer.parseInt(lines[0]);
				String fullName = lines[1];
				int bithdayYear = Integer.parseInt(lines[2]);
				int rating = Integer.parseInt(lines[3]);
				clients.add(new Client(id, fullName, bithdayYear, rating));
				line = reader.readLine();
			}
			return clients;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean writeToFile(List<Client> clients){
		try(FileWriter fileWriter = new FileWriter(WAY, false)){
			for (Client client : clients) {
				fileWriter.write(client.toString() + "\n");
			}
			return true;
		}catch (Exception e) {
			return false;
		}
	}
}
