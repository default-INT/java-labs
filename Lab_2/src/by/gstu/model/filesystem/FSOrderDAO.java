package by.gstu.model.filesystem;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.gstu.model.OrderDAO;
import by.gstu.model.entity.Order;
import by.gstu.model.mysql.ConstConfig;

public class FSOrderDAO implements OrderDAO {

	private static final String WAY = ConstConfig.FS_ORDERS;

	public FSOrderDAO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Order order) {
		List<Order> orders = readAll();
		int maxId = 1;
		if (orders != null){
			for (Order iOrder : orders){
				maxId = iOrder.getId() > maxId ? iOrder.getId() : maxId;
			}
		}else {
			orders = new ArrayList<Order>();
		}
		order.setId(++maxId);
		orders.add(order);
		return writeToFile(orders);
	}

	@Override
	public boolean update(Order order) {
		List<Order> orders = readAll();
		for (int i = 0; i < orders.size(); i++){
			if (orders.get(i).getId() == order.getId()){
				orders.set(i, order);
				return writeToFile(orders);
			}
		}
		return false;
	}

	@Override
	public Order read(int id) {
		try (FileReader fileReader = new FileReader(WAY)){
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			while (line != null) {
				String[] lines = line.split("_");
				int idLines = Integer.parseInt(lines[0]);
				if (idLines == id) {
					int clientId = Integer.parseInt(lines[1]);
					int carId = Integer.parseInt(lines[2]);
					String passportData = lines[3];
					Date dateOrder = new SimpleDateFormat("dd.mm.yyyy").parse(lines[4]);
					int rentalPeriod = Integer.parseInt(lines[5]);
					double price = Double.parseDouble(lines[6]);
					return new Order(idLines, clientId, carId, passportData, dateOrder, rentalPeriod, price);
				}
				line = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		List<Order> orders = readAll();
		if (orders != null){
			for (int i = 0; i < orders.size(); i++){
				if (orders.get(i).getId() == id){
					orders.remove(i);
					return writeToFile(orders);
				}
			}
		}
		return false;
	}

	@Override
	public boolean delete(Order order) {
		return delete(order.getId());
	}

	@Override
	public List<Order> readAll() {
		try (FileReader fileReader = new FileReader(WAY)){
			List<Order> orders = new ArrayList<Order>();
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			while (line != null) {
				String[] lines = line.split("_");
				int idLines = Integer.parseInt(lines[0]);
				int clientId = Integer.parseInt(lines[1]);
				int carId = Integer.parseInt(lines[2]);
				String passportData = lines[3];
				Date dateOrder = new SimpleDateFormat("dd.mm.yyyy").parse(lines[4]);
				int rentalPeriod = Integer.parseInt(lines[5]);
				double price = Double.parseDouble(lines[6]);
				orders.add(new Order(idLines, clientId, carId, passportData, dateOrder, rentalPeriod, price));
				line = reader.readLine();
			}
			return orders;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean writeToFile(List<Order> orders){
		try(FileWriter fileWriter = new FileWriter(WAY, false)){
			for (Order order : orders) {
				fileWriter.write(order.toString() + "\n");
			}
			return true;
		}catch (Exception e) {
			return false;
		}
	}

}
