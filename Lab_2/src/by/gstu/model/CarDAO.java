package by.gstu.model;

import java.util.List;

import by.gstu.model.entity.Car;

public interface CarDAO {
	boolean create(Car car);
	boolean update(Car car);
	Car read(int id);
	boolean delete(int id);
	
	boolean delete(Car car);
	List<Car> readAll();
}
