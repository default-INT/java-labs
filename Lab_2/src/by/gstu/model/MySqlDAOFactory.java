package by.gstu.model;

import by.gstu.model.mysql.MySqlCarDAO;
import by.gstu.model.mysql.MySqlClientDAO;
import by.gstu.model.mysql.MySqlOrderDAO;

public class MySqlDAOFactory extends DAOFactory {

	public MySqlDAOFactory() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public CarDAO getCarDAO() {
		return new MySqlCarDAO();
	}

	@Override
	public ClientDAO getClientDAO() {
		return new MySqlClientDAO();
	}

	@Override
	public OrderDAO getOrderDAO() {
		return new MySqlOrderDAO();
	}
}
