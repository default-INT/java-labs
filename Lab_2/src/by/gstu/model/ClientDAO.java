package by.gstu.model;

import java.util.List;

import by.gstu.model.entity.Client;

public interface ClientDAO {
	boolean create(Client client);
	boolean update(Client client);
	Client read(int id);
	boolean delete(int id);
	
	boolean delete(Client client);
	List<Client> readAll();
}
