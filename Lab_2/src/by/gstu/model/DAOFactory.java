package by.gstu.model;

public abstract class DAOFactory {
	private static enum DataBase{
		MYSQL, FILE_SYSTEMS
	}
	
	public abstract CarDAO getCarDAO();
	public abstract ClientDAO getClientDAO();
	public abstract OrderDAO getOrderDAO();
	
	public static DAOFactory getDAOFactory(String dataBase) {
		DataBase db = DataBase.valueOf(dataBase.toUpperCase());
		switch (db) {
			case MYSQL: return new MySqlDAOFactory(); 
			case FILE_SYSTEMS: return new FileSystemDAOFactory();
			default: return null;
		}
	}
}
