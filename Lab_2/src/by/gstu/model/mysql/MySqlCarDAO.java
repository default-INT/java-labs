/**
 * 
 */
package by.gstu.model.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.gstu.model.CarDAO;
import by.gstu.model.entity.Car;

/**
 * @author default-INT
 *
 */
public class MySqlCarDAO implements CarDAO {

	private final String CREATE = ConstMySQL.CarsSQL.CREATE;
	private final String READ = ConstMySQL.CarsSQL.READ;
	private final String READ_ALL = ConstMySQL.CarsSQL.READ_ALL;
	private final String UPDATE = ConstMySQL.CarsSQL.UPDATE;
	private final String DELETE = ConstMySQL.CarsSQL.DELETE;
	
	public MySqlCarDAO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Car car) {
		// TODO Auto-generated method stub
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(CREATE);
			pStatement.setString(1, car.getModel());
			pStatement.setInt(2, car.getMileage());
			pStatement.setInt(3, car.getYearOfIssue());
			pStatement.setDouble(4, car.getPriceHour());
            int k = pStatement.executeUpdate();
            if (k > 0) return true;
            return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return false;
	}

	@Override
	public boolean update(Car car) {
		// TODO Auto-generated method stub
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(UPDATE);
			pStatement.setString(1, car.getModel());
			pStatement.setInt(2, car.getMileage());
			pStatement.setInt(3, car.getYearOfIssue());
			pStatement.setDouble(4, car.getPriceHour());
			pStatement.setInt(5, car.getId());

            int k = pStatement.executeUpdate();
            if (k > 0) return true;
            return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return false;
	}

	@Override
	public Car read(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(READ);
			pStatement.setInt(1, id);
			ResultSet resultSet = pStatement.executeQuery();
			while(resultSet.next()) {
				String model = resultSet.getString("model");
				int mileage = resultSet.getInt("mileage");
				int yearOfIssue = resultSet.getInt("year_of_issue");
				double priceHour = resultSet.getDouble("price_hour");
				return new Car(id, model, mileage, yearOfIssue, priceHour);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(DELETE);
			pStatement.setInt(1, id);

            int k = pStatement.executeUpdate();
            if (k > 0) return true;
            return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return false;
	}

	@Override
	public boolean delete(Car car) {
		return delete(car.getId());
	}

	@Override
	public List<Car> readAll() {
		// TODO Auto-generated method stub
		List<Car> cars = new ArrayList<Car>();
		Connection connection = null;
		Statement statement = null;
		try {
			connection = ConnectionPool.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(READ_ALL);
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String model = resultSet.getString("model");
				int mileage = resultSet.getInt("mileage");
				int yearOfIssue = resultSet.getInt("year_of_issue");
				double priceHour = resultSet.getDouble("price_hour");
				cars.add(new Car(id, model, mileage, yearOfIssue, priceHour));
			}
			return cars;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return null;
	}

}
