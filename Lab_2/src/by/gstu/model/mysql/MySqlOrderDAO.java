package by.gstu.model.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.gstu.model.OrderDAO;
import by.gstu.model.entity.Order;

/**
 * @author default-INT
 *
 */
public class MySqlOrderDAO implements OrderDAO {

	private final String CREATE = ConstMySQL.OrdersSQL.CREATE;
	private final String READ = ConstMySQL.OrdersSQL.READ;
	private final String READ_ALL = ConstMySQL.OrdersSQL.READ_ALL;
	private final String UPDATE = ConstMySQL.OrdersSQL.UPDATE;
	private final String DELETE = ConstMySQL.OrdersSQL.DELETE;
	
	public MySqlOrderDAO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Order order) {
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(CREATE);
			pStatement.setInt(1, order.getClientId());
			pStatement.setInt(2, order.getCarId());
			pStatement.setString(3, order.getPassportData());
			pStatement.setDate(4, new java.sql.Date(order.getDateOrder().getTime()));
			pStatement.setInt(5, order.getRentalPeriod());
			pStatement.setDouble(6, order.getPrice());

			int k = pStatement.executeUpdate();
			if (k > 0) return true;
			return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return false;
	}

	@Override
	public boolean update(Order order) {
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(UPDATE);
			pStatement.setInt(1, order.getClientId());
			pStatement.setInt(2, order.getCarId());
			pStatement.setString(3, order.getPassportData());
			pStatement.setDate(4, (Date) order.getDateOrder());
			pStatement.setInt(5, order.getRentalPeriod());
			pStatement.setDouble(6, order.getPrice());
			pStatement.setInt(7, order.getId());

			int k = pStatement.executeUpdate();
			if (k > 0) return true;
			return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return false;
	}

	@Override
	public Order read(int id) {
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(READ);
			pStatement.setInt(1, id);
			ResultSet resultSet = pStatement.executeQuery();
			while(resultSet.next()) {
				int clientId = resultSet.getInt("client_id");
				int carId = resultSet.getInt("car_id");
				String passportData = resultSet.getString("passport_data");
				Date dateOrder = resultSet.getDate("date_order");
				int rentalPeriod = resultSet.getInt("rental_period");
				double price = resultSet.getDouble("price");
				return new Order(id, clientId, carId, passportData, dateOrder, rentalPeriod, price);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		Connection connection = null;
		PreparedStatement pStatement = null;
		try {
			connection = ConnectionPool.getConnection();
			pStatement = connection.prepareStatement(DELETE);
			pStatement.setInt(1, id);
			int k = pStatement.executeUpdate();
			if (k > 0) return true;
			return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return false;
	}

	@Override
	public boolean delete(Order order) {
		return delete(order.getId());
	}

	@Override
	public List<Order> readAll() {
		List<Order> orders = new ArrayList<Order>();
		Connection connection = null;
		Statement statement = null;
		try {
			connection = ConnectionPool.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(READ_ALL);
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				int clientId = resultSet.getInt("client_id");
				int carId = resultSet.getInt("car_id");
				String passportData = resultSet.getString("passport_data");
				Date dateOrder = resultSet.getDate("date_order");
				int rentalPeriod = resultSet.getInt("rental_period");
				double price = resultSet.getDouble("price");
				orders.add(new Order(id, clientId, carId, passportData, dateOrder, rentalPeriod, price));
			}
			return orders;
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			ConnectionPool.closeConnection(connection);
		}
		return null;
	}

}
