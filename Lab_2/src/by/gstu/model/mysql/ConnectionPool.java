package by.gstu.model.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

class ConnectionPool {
	
	private static BlockingQueue<Connection> connectionQueue;

    private static final String URL = ConstConfig.DB_URL;
    private static final String USER = ConstConfig.DB_USER;
    private static final String PASSWORD = ConstConfig.DB_PASSWORD;

    private static final int POOL_SIZE = ConstConfig.POOL_SIZE;

    static {
        connectionQueue = new ArrayBlockingQueue<Connection>(POOL_SIZE);
        for (int i = 0; i < POOL_SIZE; i++) {
            Connection connection = null;
            
            try {
            	Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(URL,  USER, PASSWORD);
                connectionQueue.offer(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    public static Connection getConnection() throws InterruptedException {
        return connectionQueue.take();
    }
    
    public static void closeConnection(Connection connection) {
        if (connection != null){
            connectionQueue.offer(connection);
        }
    }

}
