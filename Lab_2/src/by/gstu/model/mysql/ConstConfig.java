package by.gstu.model.mysql;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

final public class ConstConfig {
	private static final String PATH_TO_PROPERTIES = "src/resources/config.properties";
	private static Properties prop = new Properties();
	
	public static final String DB_URL;
	public static final String DB_USER;
	public static final String DB_PASSWORD;
	public static final int POOL_SIZE;
	
	public static final String FS_CARS;
	public static final String FS_CLIENTS;
	public static final String FS_ORDERS;
	
	static {
	    try(FileInputStream fileInputStream = new FileInputStream(PATH_TO_PROPERTIES)) {
	        prop.load(fileInputStream);
	
	        
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }finally {
	    	DB_URL = prop.getProperty("db.url");
	        DB_USER = prop.getProperty("db.user");
	        DB_PASSWORD = prop.getProperty("db.password");
	        POOL_SIZE = Integer.parseInt(prop.getProperty("con.poolSize"));
	        
	        FS_CARS = prop.getProperty("fs.cars");
	        FS_CLIENTS = prop.getProperty("fs.clients");
	        FS_ORDERS = prop.getProperty("fs.orders");
		}
	    
	
	}
}
