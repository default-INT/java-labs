package by.gstu.model.mysql;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

final class ConstMySQL {
	private static final String PATH_TO_PROPERTIES = "src/resources/mysql.properties";
	private static Properties prop = new Properties();
    public static String SELECT;

    static {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            prop.load(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static class CarsSQL {
    	public final static String CREATE;
    	public final static String READ;
    	public final static String READ_ALL;
    	public final static String UPDATE;
    	public final static String DELETE;
    	
    	static {
    		CREATE = prop.getProperty("sql.Cars.create");
    		READ = prop.getProperty("sql.Cars.read");
    		READ_ALL = prop.getProperty("sql.Cars.readAll");
    		UPDATE = prop.getProperty("sql.Cars.update");
    		DELETE = prop.getProperty("sql.Cars.delete");
    	}
    }
    
    public static class ClientsSQL {
    	public final static String CREATE;
    	public final static String READ;
    	public final static String READ_ALL;
    	public final static String UPDATE;
    	public final static String DELETE;
    	
    	static {
    		CREATE = prop.getProperty("sql.Clients.create");
    		READ = prop.getProperty("sql.Clients.read");
    		READ_ALL = prop.getProperty("sql.Clients.readAll");
    		UPDATE = prop.getProperty("sql.Clients.update");
    		DELETE = prop.getProperty("sql.Clients.delete");
    	}
	}
    
    public static class OrdersSQL {
    	public final static String CREATE;
    	public final static String READ;
    	public final static String READ_ALL;
    	public final static String UPDATE;
    	public final static String DELETE;
    	
    	static {
    		CREATE = prop.getProperty("sql.Orders.create");
    		READ = prop.getProperty("sql.Orders.read");
    		READ_ALL = prop.getProperty("sql.Orders.readAll");
    		UPDATE = prop.getProperty("sql.Orders.update");
    		DELETE = prop.getProperty("sql.Orders.delete");
    	}
	}
}
