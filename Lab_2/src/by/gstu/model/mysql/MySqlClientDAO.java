package by.gstu.model.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.gstu.model.ClientDAO;
import by.gstu.model.entity.Client;

/**
 * @author default-INT
 *
 */
public class MySqlClientDAO implements ClientDAO {

    private final String CREATE = ConstMySQL.ClientsSQL.CREATE;
    private final String READ = ConstMySQL.ClientsSQL.READ;
    private final String READ_ALL = ConstMySQL.ClientsSQL.READ_ALL;
    private final String UPDATE = ConstMySQL.ClientsSQL.UPDATE;
    private final String DELETE = ConstMySQL.ClientsSQL.DELETE;

    public MySqlClientDAO() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean create(Client client) {
        Connection connection = null;
        PreparedStatement pStatement = null;
        try {
            connection = ConnectionPool.getConnection();
            pStatement = connection.prepareStatement(CREATE);
            pStatement.setString(1, client.getFullName());
            pStatement.setInt(2, client.getBithdayYear());
            pStatement.setInt(3, client.getRating());

            int k = pStatement.executeUpdate();
            if (k > 0) return true;
            return false;
        }catch (SQLException e) {
            e.printStackTrace();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            ConnectionPool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public boolean update(Client client) {
        // TODO Auto-generated method stub
        Connection connection = null;
        PreparedStatement pStatement = null;
        try {
            connection = ConnectionPool.getConnection();
            pStatement = connection.prepareStatement(UPDATE);
            pStatement.setString(1, client.getFullName());
            pStatement.setInt(2, client.getBithdayYear());
            pStatement.setInt(3, client.getRating());
            pStatement.setInt(4, client.getId());

            int k = pStatement.executeUpdate();
            if (k > 0) return true;
            return false;
        }catch (SQLException e) {
            e.printStackTrace();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            ConnectionPool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public Client read(int id) {
        // TODO Auto-generated method stub
        Connection connection = null;
        PreparedStatement pStatement = null;
        try {
            connection = ConnectionPool.getConnection();
            pStatement = connection.prepareStatement(READ);
            pStatement.setInt(1, id);
            ResultSet resultSet = pStatement.executeQuery();
            while(resultSet.next()) {
                String fullName = resultSet.getString("fullname");
                int bithdayYear = resultSet.getInt("bithday_year");
                int rating = resultSet.getInt("rating");
                return new Client(id, fullName, bithdayYear, rating);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            ConnectionPool.closeConnection(connection);
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        // TODO Auto-generated method stub
        Connection connection = null;
        PreparedStatement pStatement = null;
        try {
            connection = ConnectionPool.getConnection();
            pStatement = connection.prepareStatement(DELETE);
            pStatement.setInt(1, id);

            int k = pStatement.executeUpdate();
            if (k > 0) return true;
            return false;
        }catch (SQLException e) {
            e.printStackTrace();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            ConnectionPool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public boolean delete(Client client) {
        // TODO Auto-generated method stub
        return delete(client.getId());
    }

    @Override
    public List<Client> readAll() {
        // TODO Auto-generated method stub
        List<Client> clients = new ArrayList<Client>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = ConnectionPool.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(READ_ALL);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String fullName = resultSet.getString("fullname");
                int bithdayYear = resultSet.getInt("bithday_year");
                int rating = resultSet.getInt("rating");
                clients.add(new Client(id, fullName, bithdayYear, rating));
            }
            return clients;
        }catch (SQLException e) {
            e.printStackTrace();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            ConnectionPool.closeConnection(connection);
        }
        return null;
    }

}
