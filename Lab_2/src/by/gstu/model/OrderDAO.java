package by.gstu.model;

import java.util.List;

import by.gstu.model.entity.Order;

public interface OrderDAO {
	
	boolean create(Order order);
	boolean update(Order order);
	Order read(int id);
	boolean delete(int id);
	
	boolean delete(Order order);
	List<Order> readAll();
}
