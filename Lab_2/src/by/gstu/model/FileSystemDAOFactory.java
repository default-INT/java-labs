package by.gstu.model;

import by.gstu.model.filesystem.FSCarDAO;
import by.gstu.model.filesystem.FSClientDAO;
import by.gstu.model.filesystem.FSOrderDAO;

public class FileSystemDAOFactory extends DAOFactory {

	public FileSystemDAOFactory() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public CarDAO getCarDAO() {
		// TODO Auto-generated method stub
		return new FSCarDAO();
	}

	@Override
	public ClientDAO getClientDAO() {
		// TODO Auto-generated method stub
		return new FSClientDAO();
	}

	@Override
	public OrderDAO getOrderDAO() {
		// TODO Auto-generated method stub
		return new FSOrderDAO();
	}

}
